# Carousel sample #

## Purpose ##

This is a pure JavaScript & CSS implementation of a carousel.

Features:

* Pages flip horizontally
* Dynamic sizing
* Page indicator
* Pages can be dragged on touch screens
* Buttons can be toggled and are centered vertically

## Usage ##

In order to run this sample, please invoke the local webserver:

    npm install
    npm start

Alternatively, [`index.html`](public/index.html) can simply be opened.

## Motivation ##

The carousel was implemented bearing in mind:

* Easily customize placement, size and page contents in HTML & CSS.
* Adding new carousels is fairly easy with a single JavaScript invocation. This could be improved by searching the DOM for carousel components, but could be slow.
* By avoiding use of libraries such as JQuery, this sample is fairly lightweight.
* Animations are very smooth due to using CSS transitions, as opposed to animating with JavaScript.

## Improvements ##

* Customizing the behavior, for instance to simply fading between pages, has not been given any consideration and would require substantial code changes. A primary focus was to handle touch events for dragging pages.
* Using custom elements would fare well with CSS isolation and code invocation (in the style of Polymer). In order to keep the dependencies simple, a pure JS/CSS approach was taken for this sample.
* Improved browser support, as this implementation hasn't been tested well.
