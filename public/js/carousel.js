document.addEventListener("DOMContentLoaded", function(e) {
	// Set up the main carousel
	var carousel = document.getElementById('carousel-fullscreen');
	set_up_carousel(carousel, 8000, function(i){alert('navigate to:\n' + i);});

	// Set up the extra carousel
	var carousel = document.getElementById('carousel-extras');
	set_up_carousel(carousel, 4000);

	// Set up the social carousel
	var carousel = document.getElementById('carousel-social');
	set_up_carousel(carousel);
});

// Test and monitor media query for touch screen
var touchscreen = false;
function touchscreenTest(x) {
	touchscreen = x.matches;
}
var x = window.matchMedia("(pointer: coarse)");
touchscreenTest(x);
x.addListener(touchscreenTest);

function set_up_carousel(carousel, autoAdvance = 0, itemClick = null) {
	var items = carousel.getElementsByClassName('items')[0],
		dots = carousel.getElementsByClassName('dots')[0],
		prevBtn = carousel.getElementsByClassName('prev')[0],
		nextBtn = carousel.getElementsByClassName('next')[0];

	var dragOffsetStart = 0,
		dragOffsetCur = 0,
		dragThreshold = 100,
		pageIndex = 0,
		pageWidth = 0;

	var pages = items.children,
		pageCount = pages.length;

	// Prepare dot indicators
	if (dots) {
		for (let page of pages) {
			dots.appendChild(document.createElement('a'));
		}
		dots.children[0].classList.add('selected');
	}

	var firstPage = pages[0],
		lastPage = pages[pageCount - 1],
		cloneFirst = firstPage.cloneNode(true),
		cloneLast = lastPage.cloneNode(true);

	if (itemClick) {
		for (let page of pages) {
			if (page.getAttribute('data-url')) {
				page.style.cursor = 'pointer';
				page.onclick = function(){
					itemClick(page.getAttribute('data-url'));
				};
			}
		}
	}

	// Monitor size changes
	window.addEventListener("resize", onResize);
	onResize(null);

	if (pageCount <= 1) {
		if (prevBtn) prevBtn.style.display = 'none';
		if (nextBtn) nextBtn.style.display = 'none';
		return;
	}
	
	// Clone first and last page
	items.appendChild(cloneFirst);
	items.insertBefore(cloneLast, firstPage);

	// Mouse and Touch events
	items.onmousedown = onDragStart;
	
	// Touch events
	items.addEventListener('touchstart', onDragStart);
	items.addEventListener('touchend', onDragEnd);
	items.addEventListener('touchmove', onDragMove);

	// Click events
	if (prevBtn) prevBtn.addEventListener('click', function () { flipPage(-1) });
	if (nextBtn) nextBtn.addEventListener('click', function () { flipPage(1) });

	// Transition events
	items.addEventListener('transitionend', settlePage);

	// Automatically advance the pages
	if (autoAdvance > 0) {
		function autoFlip() {
			setTimeout(function(){
					flipPage(1);
					autoFlip();
				}, autoAdvance);
		}
		autoFlip();
	}

	// Callback when dragging starts
	function onDragStart(e) {
		if (!touchscreen) return;

		e = e || window.event;
		e.preventDefault();

		// Ensure settled
		settlePage();

		dragOffsetStart = items.offsetLeft;
		if (e.type == 'touchstart') {
			dragOffsetCur = e.touches[0].clientX;
		} else {
			dragOffsetCur = e.clientX;
			// Start observing events
			document.onmousemove = onDragMove;
			document.onmouseup = onDragEnd;
		}
	}

	// Callback when dragging
	function onDragMove(e) {
		e = e || window.event;
		if (e.type == 'touchmove') {
			clientX = e.touches[0].clientX;
		} else {
			clientX = e.clientX;
		}
		offset = dragOffsetCur - clientX;
		dragOffsetCur = clientX;
		items.style.left = (items.offsetLeft - offset) + "px";
	}
	
	// Callback when dragging completes
	function onDragEnd(e) {
		// Determine if we should flip to the next page
		dragDistance = items.offsetLeft - dragOffsetStart;

		// TODO also allow flipping by measuring velocity
		if (dragDistance < -dragThreshold) {
			// Dragged left; flip to the next page
			flipPage(1);
		} else if (dragDistance > dragThreshold) {
			// Dragged rightl flip to the previous page
			flipPage(-1);
		} else {
			// Return page to old position
			flipPage(0);
		}

		// Stop observing events
		document.onmouseup = null;
		document.onmousemove = null;
	}

	function onResize(e) {
		pageWidth = carousel.offsetWidth;
		for (let page of pages) {
			page.style.width = pageWidth + "px";
		}
		cloneFirst.style.width = pageWidth + "px";
		cloneLast.style.width = pageWidth + "px";
		dragThreshold = pageWidth * 0.1;
		if (dots) {
			dots.style.top = carousel.offsetHeight + "px";
		}
		if (prevBtn) prevBtn.style.top = (carousel.offsetHeight - prevBtn.offsetHeight) / 2 + "px";
		if (nextBtn) nextBtn.style.top = (carousel.offsetHeight - nextBtn.offsetHeight) / 2 + "px";
		settlePage();
	}

	function flipPage(dir) {
		// Only flip if not already animating
		if (!items.classList.contains('transition')) {
			items.classList.add('transition');
			if (dots) {
				dots.children[pageIndex].classList.remove('selected');
			}
			if (dir == 1) {
				pageIndex++;
			} else if (dir == -1) {
				pageIndex--;
			}
			// First animate to the correct page (accommodating for start and end overflow pages)
			items.style.left = -((pageIndex+1) * pageWidth) + "px";
			// Only now correct the page index within the page count
			if (pageIndex == -1) {
				pageIndex = pageCount - 1;
			} else {
				while (pageIndex >= pageCount) {
					pageIndex -= pageCount;
				}
			}
			if (dots) {
				dots.children[pageIndex].classList.add('selected');
			}
		}
	}

	function settlePage() {
		// Intercept transition
		items.classList.remove('transition');
		if (pageCount <= 1) {
			items.style.left = 0;
		} else {
			items.style.left = -((pageIndex+1) * pageWidth) + "px";
		}
	}
}
